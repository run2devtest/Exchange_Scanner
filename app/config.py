import configparser
import ccxt

from os import path

BASE_PATH = path.dirname(path.abspath(__file__))
CONFIG_FILE = path.join(BASE_PATH, 'settings.ini')


def getExchanges():
    return ccxt.exchanges


def createConfig():
    config = configparser.ConfigParser()
    config['SETTINGS'] = {'SLEEP_TIME_MINUTES': '5', 'SEND_ALERTS': False}
    config['TOKEN'] = {'TELEGRAM': ''}
    config['API'] = {}
    for exchg in getExchanges():
        config['API'][exchg] = ''

    with open(CONFIG_FILE, 'w') as configfile:
        config.write(configfile)


def readConfig():
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    return config


def getApiKeys(key):
    config = readConfig()
    return config['API'][key]


def getSleepTime():
    config = readConfig()
    return config['SETTINGS']['SLEEP_TIME_MINUTES']


def getSendAlertSetting():
    config = readConfig()
    return config['SETTINGS'].getboolean('SEND_ALERTS')


def main():
    createConfig()
    print(type(getSendAlertSetting()))

if __name__ == '__main__':
    main()
