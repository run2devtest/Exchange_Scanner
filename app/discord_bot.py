from dhooks.discord_hooks import Webhook
from time import sleep

main_web_hook_url = 'https://discordapp.com/api/webhooks/414878709590523905/dlDdKcfAv5dI9itXzGsOSGefapAHDyPiH959jIhwj8_ZLsIFmYL-uOcm9tO030hODPpd'
secondary_web_hook_url = 'https://discordapp.com/api/webhooks/414879136105103373/2yPFB3h59OwgLVXICcXmzjiVDSjCGpOpjLiFwRH8KMASXa5hKGoOdB7MtJvlNnilgmGM'
highvolume_web_hook_url = 'https://discordapp.com/api/webhooks/417130113449328659/vrBiL9dmUvzJlNe5I0X3IzNI8dnS1eFUF3K_jXB03ImB-oAKh6mMR9-uLczrvFxmyPKr'

def discord_exception(f):
    def wrapper(*args, **kw):
        try:
            return f(*args, **kw)

        except Exception as e:
            print(e, 'Error Detected')
            sleep(30)

    return wrapper


@discord_exception
def get_chat_channel(exchange):
    exhange_addresses = {'binance': main_web_hook_url,
                         'bittrex': main_web_hook_url,
                         'poloniex': main_web_hook_url,
                         'kucoin': highvolume_web_hook_url,
                         'hitbtc': highvolume_web_hook_url,
                         'hitbtc2': highvolume_web_hook_url,
                         'okex': highvolume_web_hook_url,
                         'huobipro': highvolume_web_hook_url
                         }
    try:
        return exhange_addresses[exchange]
    except KeyError:
        return secondary_web_hook_url


@discord_exception
def sendNewCoinMessage(exchange, pair):
    message = 'Exchange: {}' + '\n'
    message += 'Symbol: {}' + '\n'
    message += 'New Coin Pair Found.' + '\n'
    message = message.format(exchange.upper(), pair.upper())

    msg = Webhook(get_chat_channel(exchange), msg='```{}```'.format(message))
    msg.post()


@discord_exception
def sendHaltMessage():
    message = '```COIN SEARCH BOT HALTED```'
    msg = Webhook(main_web_hook_url, msg=message)
    msg.post()
    msg = Webhook(secondary_web_hook_url, msg=message)
    msg.post()


@discord_exception
def sendStartMessage():
    message = '```COIN SEARCH BOT STARTED```'
    msg = Webhook(main_web_hook_url, msg=message)
    msg.post()
    msg = Webhook(secondary_web_hook_url, msg=message)
    msg.post()

if __name__ == '__main__':
    sendNewCoinMessage('bittrex', 'test')
    sendNewCoinMessage('kucoin', 'test')
