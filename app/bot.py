import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Job
from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)

from time import sleep

from gettoken import GetToken


def telegram_exception(f):
    def wrapper(*args, **kw):
        try:
            return f(*args, **kw)

        except Unauthorized as e:
            print(e, 'remove update. message.chat_id from conversation list')
            sleep(30)

        except BadRequest as e:
            print(e, 'handle malformed requests - read more below!')
            sleep(30)

        except TimedOut as e:
            print(e, 'handle slow connection problems')
            sleep(30)

        except NetworkError as e:
            print(e, 'handle other connection problems')
            sleep(30)

        except ChatMigrated as e:
            print(e, 'the chat_id of a group has changed, use e.new_chat_id instead')
            sleep(30)

        except TelegramError as e:
            print(e, 'handle all other telegram related errors')
            sleep(30)

        except Exception as e:
            print(e, 'Exception')
            sleep(30)

    return wrapper


def get_chat_channel(exchange):
    exhange_addresses = {'binance': '-1001320163374',
                         'bittrex': '-1001320163374',
                         'poloniex': '-1001320163374'}
    try:
        return exhange_addresses[exchange]
    except KeyError:
        return '-1001383826430'


@telegram_exception
def sendNewCoinMessage(exchange, pair):
    bot = telegram.Bot(GetToken())
    message = 'Exchange: {}' + '\n'
    message += 'Symbol: {}' + '\n'
    message += 'New Coin Pair Found.' + '\n'
    message = message.format(exchange.upper(), pair.upper())

    bot.send_message(chat_id=get_chat_channel(exchange), text=message)


@telegram_exception
def sendHaltMessage():
    bot = telegram.Bot(GetToken())
    message = 'COIN SEARCH BOT HALTED'
    bot.send_message(chat_id='-1001320163374', text=message)
    bot.send_message(chat_id='-1001383826430', text=message)


@telegram_exception
def sendStartMessage():
    bot = telegram.Bot(GetToken())
    message = 'COIN SEARCH BOT STARTED'
    bot.send_message(chat_id='-1001320163374', text=message)
    bot.send_message(chat_id='-1001383826430', text=message)
